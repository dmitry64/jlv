pub enum LogLevel {
    Info,
    Debug,
    Warning,
    Error,
    Trace,
    Fatal,
    Other(String),
    Unknown,
}

impl LogLevel {
    pub fn to_string(&self) -> String {
        match self {
            LogLevel::Info => String::from("INFO "),
            LogLevel::Debug => String::from("DEBUG"),
            LogLevel::Warning => String::from("WARN "),
            LogLevel::Error => String::from("ERROR"),
            LogLevel::Trace => String::from("TRACE"),
            LogLevel::Fatal => String::from("FATAL"),
            LogLevel::Other(string) => string.to_uppercase(),
            LogLevel::Unknown => String::from(""),
        }
    }
}
