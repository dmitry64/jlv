use crate::timefield::*;
use crate::loglevel::*;


pub struct LogLine 
{
    pub log_level: LogLevel,
    pub timefields: Vec<Timefield>,
    pub message: Option<String>,
    pub thread: Option<String>,
    pub other_fields: Vec<String>,    
}

