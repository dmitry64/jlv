mod arguments;
mod configuration;
mod fieldtype;
mod filereader;
mod lineparser;
mod lineprinter;
mod loglevel;
mod logline;
mod parseresult;
mod printcolor;
mod printstyle;
mod timefield;

use crate::arguments::*;
use crate::filereader::*;

use std::io::{self, prelude::*};

fn main() -> std::io::Result<()> {
    match Arguments::parse_cli_arguments() {
        Ok(config) => {
            FileReader::read_file(&config);
            io::stdout().flush().unwrap();
            Ok(())
        }
        Err(err) => Err(io::Error::new(
            io::ErrorKind::Other,
            format!("Failed to parse CLI arguments: {}", err),
        )),
    }
}
