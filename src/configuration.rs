use crate::fieldtype::*;

pub struct FieldConfig {
    pub key: String,
    pub field_type: FieldType,
}

pub struct Configuration {
    pub path: String,
    pub follow: bool,
    pub no_color: bool,
    pub num_lines: u32,
    pub field_overrides: Vec<FieldConfig>,
}

impl Configuration {
    pub fn new(
        path: String,
        follow: bool,
        no_color: bool,
        num_lines: u32,
        field_overrides: Vec<FieldConfig>,
    ) -> Configuration {
        Configuration {
            path,
            follow,
            no_color,
            num_lines,
            field_overrides,
        }
    }
}
