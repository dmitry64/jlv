use crate::logline::*;

pub enum ParseResult {
    ParsedLine(LogLine),
    RawString(String),
    None
}