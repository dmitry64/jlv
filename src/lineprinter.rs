use crate::configuration::*;
use crate::loglevel::*;
use crate::logline::*;
use crate::parseresult::*;

use colored::*;
use std::io::{self, prelude::*};

pub struct LinePrinter {}

impl LinePrinter {
    pub fn print_line(parse_result: &ParseResult, config: &Configuration) {
        match parse_result {
            ParseResult::ParsedLine(parsed_line) => {
                LinePrinter::print_parsed_line(parsed_line, config)
            }
            ParseResult::RawString(raw_line) => {
                println!("Invalid JSON line [{}]", raw_line.replace("\n", "").red())
            }
            ParseResult::None => {}
        }
        io::stdout().flush().unwrap();
    }

    pub fn print_parsed_line(logline: &LogLine, config: &Configuration) {
        let mut print: Vec<ColoredString> = vec![];

        for timefield in &logline.timefields {
            print.push(format!("[{}]", timefield.to_string()).dimmed());
        }

        match logline.log_level {
            LogLevel::Unknown => {}
            _ => {
                print.push(format!("[{}]", logline.log_level.to_string()).dimmed());
            }
        }

        match &logline.message {
            Some(message) => print.push(format!("[{}]", message).normal()),
            None => {}
        }

        for other_field in &logline.other_fields {
            print.push(format!("[{}]", other_field).dimmed());
        }
        for string in print {
            if config.no_color {
                print!("{}", string.normal());
            } else {
                match logline.log_level {
                    LogLevel::Debug => print!("{}", string.green()),
                    LogLevel::Error => print!("{}", string.red()),
                    LogLevel::Warning => print!("{}", string.yellow()),
                    LogLevel::Info => print!("{}", string.white()),
                    LogLevel::Trace => print!("{}", string.blue()),
                    LogLevel::Fatal => print!("{}", string.purple()),
                    LogLevel::Other(_) => print!("{}", string.white()),
                    LogLevel::Unknown => print!("{}", string.white()),
                }
            }
        }

        println!();
    }
}
