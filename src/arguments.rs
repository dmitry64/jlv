use crate::configuration::*;
use crate::fieldtype::*;
use clap::*;

pub struct Arguments {}

const APP_VERSION: &str = "0.6.0";

const DEFAULT_PRINT_LINES: u32 = 30;

impl Arguments {
    pub fn parse_cli_arguments() -> std::result::Result<Configuration, String> {
        let settings = App::new("Json Log Viewer")
            .version(APP_VERSION)
            .author("Dmitry Z. <dz64@protonmail.com>")
            .about("Tool for json logs visualization")
            .arg(Arg::with_name("follow").short("f").long("follow"))
            .arg(
                Arg::with_name("INPUT")
                    .help("Sets the input file to use")
                    .required(true)
                    .index(1),
            )
            .arg(
                Arg::with_name("num-lines")
                    .long("num-lines")
                    .short("n")
                    .takes_value(true)
                    .help("Number of lines to print from the end"),
            )
            .arg(
                Arg::with_name("config")
                    .long("config")
                    .short("c")
                    .takes_value(true)
                    .help("Field configuration"),
            )
            .arg(
                Arg::with_name("no-color")
                    .long("no-color")
                    .short("d")
                    .help("Disable colored output"),
            )
            .get_matches();

        let is_follow = settings.is_present("follow");

        let num_lines = match settings.is_present("num-lines") {
            true => match settings.value_of("num-lines") {
                Some(val) => val.parse::<u32>().unwrap(),
                None => return Err(String::from("Number of lines not specified")),
            },
            false => DEFAULT_PRINT_LINES,
        };

        let field_config: Vec<FieldConfig> = match settings.is_present("config") {
            true => match settings.value_of("config") {
                Some(val) => {
                    let parsed_config: Vec<FieldConfig> =
                        match Arguments::parse_field_config(String::from(val)) {
                            Ok(conf) => conf,
                            Err(err) => {
                                return Err(format!("{}", err));
                            }
                        };
                    parsed_config
                }
                None => {
                    return Err(String::from("Config option requires configuration string"));
                }
            },
            false => vec![],
        };

        let no_color = settings.is_present("no-color");

        let path = settings.value_of("INPUT").unwrap().to_string();
        let config = Configuration::new(path, is_follow, no_color, num_lines, field_config);

        return Ok(config);
    }

    fn parse_field_config(string: String) -> std::result::Result<Vec<FieldConfig>, String> {
        let mut result: Vec<FieldConfig> = vec![];
        let field_configs = string.split(",");
        for field_config in field_configs {
            let mut config = field_config.split(":");
            let name_str = match config.next() {
                Some(name) => name,
                None => return Err(format!("Bad name config")),
            };

            let type_str = match config.next() {
                Some(name) => name,
                None => return Err(format!("Bad type config")),
            };

            let field_type = FieldType::from_string(String::from(type_str));

            match field_type {
                FieldType::Unknown => return Err(format!("Unknown field type")),
                _ => {}
            }

            let conf = FieldConfig {
                key: String::from(name_str),
                field_type,
            };

            result.push(conf);
        }

        Ok(result)
    }
}
