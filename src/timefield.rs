pub enum Timefield {
    StringBased(String),
    None,
}

impl Timefield {
    pub fn to_string(&self) -> String {
        match self {
            Timefield::StringBased(string) => string.clone(),
            Timefield::None => String::from("none"),
        }
    }
}
