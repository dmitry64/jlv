pub enum PrintStyle {
    Normal,
    Bold,
    Underline,
    Italic,
    Dimmed,
}