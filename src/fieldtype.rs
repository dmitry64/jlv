
pub enum FieldType {
    Timefield,
    Message,
    LogLevel,
    Thread,
    OtherField,
    Skip,
    Unknown
}

impl FieldType {
    pub fn from_string(string: String) -> FieldType 
    {
        match &string[..] {
            "time" => FieldType::Timefield,
            "data" => FieldType::Message,
            "level" => FieldType::LogLevel,
            "thread" => FieldType::Thread,
            "other" => FieldType::OtherField,
            "skip" => FieldType::Skip,
            _ => FieldType::Unknown
        }
    }
}