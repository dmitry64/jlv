use crate::configuration::*;
use crate::lineparser::*;
use crate::lineprinter::*;

use std::fs::File;
use std::io::SeekFrom;
use std::io::{prelude::*, BufReader, Seek};

pub struct FileReader {}

impl FileReader {
    pub fn read_file(configuration: &Configuration) {
        let file = File::open(configuration.path.clone());
        let actual_file;
        match file {
            Ok(f) => actual_file = f,
            Err(why) => {
                println!("Failed to open file! Reason {}", why);
                return;
            }
        }
        let mut reader = BufReader::new(actual_file);

        let mut common_buffer: Vec<u8> = vec![];
        FileReader::seek_to_end(&mut reader, configuration.num_lines);
        loop {
            let mut read_buffer: Vec<u8> = vec![];
            let res = reader.read_until('\n' as u8, &mut read_buffer);
            match res {
                Ok(length) => {
                    if length == 0 {
                        if !configuration.follow {
                            return;
                        } else {
                            let poll_time = std::time::Duration::from_millis(10);
                            std::thread::sleep(poll_time);
                        }
                    }

                    common_buffer.extend(read_buffer[0..length].iter().cloned());
                    let mut might_be_lines = true;
                    while might_be_lines {
                        match FileReader::try_get_line(&common_buffer) {
                            Some(index) => {
                                let line: String = std::str::from_utf8(&common_buffer[0..index])
                                    .unwrap()
                                    .to_string();

                                common_buffer.drain(0..index);

                                if length != 0 {
                                    let result = LineParser::parse_line(&line, &configuration);
                                    LinePrinter::print_line(&result, &configuration);
                                }
                            }
                            None => {
                                might_be_lines = false;
                            }
                        }
                    }
                }
                Err(err) => println!("Error! {}", err),
            }
        }
    }

    fn seek_to_end<R>(reader: &mut BufReader<R>, num_lines: u32)
    where
        R: Seek + Read,
    {
        reader.seek(SeekFrom::End(0)).unwrap();

        let mut lines = 0;
        let mut buf = vec![0u8; 1];

        while reader.seek(SeekFrom::Current(-1)).unwrap() > 0 {
            reader.read(&mut buf).unwrap();
            if buf[0] == '\n' as u8 {
                lines += 1;
                if lines > num_lines {
                    break;
                }
            }
            reader.seek(SeekFrom::Current(-1)).unwrap();
        }
    }

    fn try_get_line(byte_array: &Vec<u8>) -> Option<usize> {
        let length = byte_array.len();
        let mut found: bool = false;
        let mut index: usize = 0;
        for i in 0..length {
            if byte_array[i] == '\n' as u8 {
                found = true;
                index = i;
                break;
            }
        }
        if found {
            return Some(index + 1);
        } else {
            return None;
        }
    }
}
