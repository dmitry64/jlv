use crate::configuration::*;
use crate::fieldtype::*;
use crate::loglevel::*;
use crate::logline::*;
use crate::parseresult::*;
use crate::timefield::*;

pub struct LineParser {}

impl LineParser {
    pub fn parse_line(line: &String, config: &Configuration) -> ParseResult {
        if line.len() < 2 {
            return ParseResult::None;
        }

        let parsed_json_result = json::parse(line);

        match parsed_json_result {
            Ok(parsed_json) => {
                let line = LineParser::create_line(&parsed_json, config);
                ParseResult::ParsedLine(line)
            }
            Err(_) => ParseResult::RawString(line.replace("\n", "")),
        }
    }

    pub fn create_line(json_object: &json::JsonValue, config: &Configuration) -> LogLine {
        let entries = json_object.entries();

        let mut log_level = LogLevel::Unknown;
        let mut timefields: Vec<Timefield> = vec![];

        let mut message: Option<String> = None;
        let mut thread: Option<String> = None;

        let mut other_fields: Vec<String> = vec![];

        for (key, value) in entries {
            let mut field_found = false;
            for field_override in &config.field_overrides {
                if field_override.key == key {
                    field_found = true;

                    match field_override.field_type {
                        FieldType::Timefield => {
                            timefields.push(LineParser::parse_time_field(&value.to_string()))
                        }
                        FieldType::Thread => thread = Some(value.to_string()),
                        FieldType::Message => message = Some(value.to_string()),
                        FieldType::LogLevel => {
                            log_level = LineParser::get_log_level(&value.to_string())
                        }
                        FieldType::OtherField => other_fields.push(format!("{}:{}", key, value)),
                        FieldType::Skip => {}
                        FieldType::Unknown => {}
                    }
                    break;
                }
            }

            if !field_found {
                let lowercase = key.to_ascii_lowercase();
                match &lowercase[..] {
                    "time" | "timestamp" | "t" | "date" | "datetime" => {
                        timefields.push(LineParser::parse_time_field(&value.to_string()));
                    }
                    "thread" | "thr" => {
                        thread = Some(value.to_string());
                    }
                    "message" | "msg" | "m" | "value" | "payload" | "data" => {
                        message = Some(value.to_string());
                    }
                    "level" | "log_level" | "lvl" | "level_name" => {
                        log_level = LineParser::get_log_level(&value.to_string());
                    }
                    _ => {
                        other_fields.push(format!("{}:{}", key, value));
                    }
                }
            }
        }

        LogLine {
            log_level,
            timefields,
            message,
            thread,
            other_fields,
        }
    }

    fn get_log_level(level: &String) -> LogLevel {
        let lowercase = level.to_ascii_lowercase();
        match &lowercase[..] {
            "info" | "i" => LogLevel::Info,
            "debug" | "d" => LogLevel::Debug,
            "warning" | "w" | "warn" => LogLevel::Warning,
            "error" | "e" | "err" => LogLevel::Error,
            "trace" | "t" => LogLevel::Trace,
            "fatal" | "f" => LogLevel::Fatal,
            string => LogLevel::Other(String::from(string)),
        }
    }

    fn parse_time_field(string: &String) -> Timefield {
        Timefield::StringBased(String::from(string))
    }
}
