# JSON Log Viewer
[![pipeline status](https://gitlab.com/dmitry64/jlv/badges/master/pipeline.svg)](https://gitlab.com/dmitry64/jlv/-/commits/master)
[![coverage report](https://gitlab.com/dmitry64/jlv/badges/master/coverage.svg)](https://gitlab.com/dmitry64/jlv/-/commits/master)
## Description
Simple JSON Log viewer written in Rust. It prints out formatted and colored log to stdout.
## Usage
JSON-lines log format is not human-readable - JLV can make it easier. Here is an example of input data in `log.txt`:
```javascript
{"time":"2019-07-14T19:43:37+0100", "level":"debug","thread": 3, "message":"Debug message", "tags":["tag2", "tag3"], "user_id":123456}
{"time":"2019-07-14T19:45:37+0100", "level":"warning","thread": 4, "message":"Warning message here", "tags":["Apple", "Banana", "Orange"], "user_id":123456}
{"time":"2019-07-14T19:45:37+0100", "level":"trace","thread": 4, "message":"Trace message", "tags":["Foo", "Bar", "foobar"]}
{"time":"2019-07-14T19:43:37+0100", "level":"error","thread": 1, "message":"Error message here", "tags":["Cat", "Dog", "Snake"], "context":{ "error": "Some error here"}}
```

### Command:

```bash
jlv ./log.txt
```

### Result:
![Alt text](/docs/screenshots/example1.png?raw=true "Output")

### Field configuration

JLV can recognize and treat differently certain special types of json fields:

* Message (`data`) - main body of a log message (printed after log level)
* Log level (`level`) - used to determine color of a log line
* Time (`time`) - time of a log message (always printed on the left side)
* Thread (`thread`) - thread from where a log was sent
* Other (`other`) - other fields (printed with field names)
* Skip (`skip`) - field that should not be printed

JLV will try to guess what field types are present in log line, unless there is an override (--config option). For example we can apply `skip` option to remove fields that we don't want to see. Using the same `log.txt` from before:
```
jlv -c context:skip,tags:skip ./log.txt
```
Result:
![Alt text](/docs/screenshots/example2.png?raw=true "Output")

### Options:

* -h (--help) - print help message with syntax info
* -f (--follow) - the program will watch for changes in the file
* -n (--num-lines) - number of lines to print from the end of the file
* -d (--no-color) - disabled colored output
* -c (--config) - configure how to interpret different fields in logs. Config example: `jlv -c some_field_name:data,my_log_level:level,useless_field:skip ./log.txt` will treat `some_field_name` as data field, `my_log_level` as log level and `useless_field` will be skipped.